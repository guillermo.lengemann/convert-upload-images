# Convert and upload images

## Requirements

Install system dependencies:

```bash
sudo apt install libwebp-dev python3-pip
```

Install python dependencies:

```bash
pip3 install -r requirements.txt
```

## How to use

Run in *dry mode* to check the changes before actually executing them. 

```bash
python3 convert-upload.py --dry-run
```

## References

- https://stackoverflow.com/questions/64211835/keyerror-webp-in-pillow-7-2-0-on-ubuntu-20-04-with-already-installed-libwebp
- https://stackoverflow.com/questions/19860639/convert-images-to-webp-using-pillow
- https://stackoverflow.com/questions/58378394/open-webp-images-in-gce-deep-learning-vm
