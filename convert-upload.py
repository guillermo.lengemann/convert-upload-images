#!/usr/bin/env python
import getopt
import glob
import logging
import os
import sys
import boto3
from PIL import Image
from boto3.exceptions import S3UploadFailedError
from dotenv import load_dotenv

bucket = ''
endpoint = ''
from_file = ''


def get_new_path(path):
    prefix = os.getenv('PREFIX_REMOVE')

    return path[len(prefix):]


def convert_upload(dryrun):
    files = glob.glob(from_file + '/**/*', recursive=True)
    files = [f for f in files if os.path.isfile(f)]

    if not files:
        print("No files found in %s" % (from_file))

    for file_name in files:
        basename = os.path.basename(file_name)
        new_file = os.path.splitext(basename)[0] + '.webp'
        dirname = os.path.dirname(file_name)
        new_path = get_new_path(dirname)
        new_full_path = new_path + '/' + new_file
        new_format_file = dirname + '/' + new_file

        print("Convert from %s to %s" % (file_name, new_format_file))
        if not dryrun:
            im = Image.open(file_name)
            im.save(new_format_file, format="webp")

        if endpoint:
            s3_client = boto3.client('s3', endpoint_url=endpoint)
        else:
            s3_client = boto3.client('s3')

        try:
            print("Uploading from %s to %s" % (new_format_file, new_full_path))
            if not dryrun:
                s3_client.upload_file(new_format_file, bucket, new_full_path)
                os.remove(new_format_file)

            if dryrun:
                print("Run in dry mode. No changes was made")
                break
        except S3UploadFailedError as e:
            os.remove(new_format_file)
            logging.error(e)
            break


def main():
    try:
        opts, args = getopt.getopt(sys.argv[1:], '', ["env=", "dry-run"])
    except getopt.GetoptError as err:
        print(err)
        sys.exit(2)

    dryrun = False
    env = 'prod'
    for o, a in opts:
        if o in '--env':
            if a == 'dev':
                env = a
        elif o in "--dry-run":
            dryrun = True
        else:
            assert False, "unhandled option"

    if env == 'dev':
        load_dotenv('.env.dev')
    else:
        load_dotenv()

    global bucket
    bucket = os.getenv('BUCKET')
    global endpoint
    endpoint = os.getenv('ENDPOINT')
    global from_file
    from_file = os.getenv('FROM')

    convert_upload(dryrun)


if __name__ == "__main__":
    main()
